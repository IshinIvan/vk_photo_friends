﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VK_FriendsPhoto
{
    /// <summary>
    /// Логика взаимодействия для Window_for_objects.xaml
    /// </summary>
    public partial class Window_for_objects : Window
    {
        public Window_for_objects(List<User_object> objects)
        {
            InitializeComponent();

            for (int i = 0; i < objects.Count; i++)
            {
                Object_1st obj = new Object_1st(objects[i]);
                listView.Items.Add(obj);
            }

        }
    }
}
