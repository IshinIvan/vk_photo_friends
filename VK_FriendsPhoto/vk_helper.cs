﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK_FriendsPhoto
{
    public static class vk_helper
    {
        public static string s_url = "https://api.vk.com/method/";
        public static string s_get_url = "friends.get";
        public static string s_get_url_ids = "users.get";
        public static string s_vers = "&v=5.8";
        public static string s_pr = "user_ids=";
        public static string s_order = "&order=random";
        public static string s_photo_max = "photo_max";
        public static string s_photo_100 = "photo_100";
        
    }
}
