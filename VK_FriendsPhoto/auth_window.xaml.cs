﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VK_FriendsPhoto
{
    /// <summary>
    /// Логика взаимодействия для auth_window.xaml
    /// </summary>
    public partial class auth_window : Window
    {
        public auth_window()
        {
            InitializeComponent();
            WebBr_auth.Navigate("https://oauth.vk.com/authorize?client_id=5317310&display=mobile&scope=friends&response_type=token&v=5.45");
        }

        public void WebBr_auth_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            if (WebBr_auth.Source != null)
            {
                string s_url_for_token = WebBr_auth.Source.ToString();
                //"https://oauth.vk.com/blank.html#access_token=
                // 80d4311195ba06ae65c2041103a7f7d7ab567f18600494ce79709e39414556cc0369acd044f60d95a3191&expires_in=86400&user_id=268213722"
                //Извлечение token & user_id
                if (s_url_for_token.Contains("token="))
                {
                    //Поиск начального элемента token-a
                    int count_begin_token = s_url_for_token.IndexOf("token=");
                    //Поиск конечного элемента token-a
                    int count_end_token = s_url_for_token.IndexOf('&');
                    //Извлечение token-a
                    token_save.s_token = s_url_for_token.Substring((count_begin_token+6), (count_end_token  - (count_begin_token+6)));
                    //Поиск начального элемента user_id
                    int count_begin_user_id = s_url_for_token.IndexOf("user_id=");
                    //Поиск конечного элемента user_id
                    int count_end_user_id = s_url_for_token.LastIndexOf('&');
                    //Проверка есть ли еще каки-либо данные в первоначальное строке после user_id
                    if (count_end_user_id > count_begin_user_id)
                    {
                        token_save.s_user_id = s_url_for_token.Substring(count_begin_user_id + 8, count_end_user_id - (count_begin_user_id + 8));
                    }
                    else
                    {
                        token_save.s_user_id = s_url_for_token.Substring((count_begin_user_id + 8),((s_url_for_token.Count()) - (count_begin_user_id + 8)));
                    }

                    this.Close();

                    /*bool f = int.TryParse(token_save.s_user_id, out token_save.user_id);
                    if (!f)
                    {

                        Error_window open_error_window = new Error_window();
                        open_error_window.Show();
                        open_error_window.error_textBox.Text = "Please log in again after a few seconds";

                    }*/
                }
             }
        }

    }
}
