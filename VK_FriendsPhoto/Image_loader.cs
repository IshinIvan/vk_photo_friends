﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace VK_FriendsPhoto
{
    public class Image_loader
    {
        public static void photo_load(Image photo_100, string s_url_photo_100)
        {
            var fullFilePath = s_url_photo_100;

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(fullFilePath, UriKind.Absolute);
            bitmap.EndInit();

            photo_100.Source = bitmap;
        }
    }
}
