﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VK_FriendsPhoto
{
    /// <summary>
    ///  Логика взаимодействия для Object_1st.xaml
    /// </summary>
    public partial class Object_1st : UserControl
    {
        public Object_1st(User_object User)
        {
            InitializeComponent();
            textBox_name.Text = User.first_name + " " + User.last_name;
            textBox_for_json_photo.Text = User.trans_scores + User.face_rectangle;
            Image_loader.photo_load(Photo_100x100, User.Image_Url);

        }
    }
}
