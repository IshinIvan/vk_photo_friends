﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK_FriendsPhoto
{
    public class json_for_post_req_emote
    {
        public Req_emote response;
    }
    public class Req_emote
    {
        public FaceRectangle faceRectangle;
        public Scores scores;
    }

    
    public class FaceRectangle
    {
        public int left;
        public int top;
        public int width;
        public int height;
        public string trans_faceRectangle()
        {
            return "left\": \"" + left.ToString() + "\"" + "\n" + "top\":" + top.ToString() + "\"" +"\n" + "width\":" + width.ToString() + "\"" + "\n" + "height\":" + height.ToString() + "\"" + "\n";
        }
    }
    public class Scores
    {
        public double anger;
        public double contempt;
        public double disgust;
        public double fear;
        public double happiness;
        public double neutral;
        public double sadness;
        public double surprise;
        public string trans_scores ()
        {
            string s1 = "anger\":" + anger.ToString() + "\"" + "\n" + "contempt\":" + contempt.ToString() + "\"" + "\n";
            s1 = s1 + "disgust\":" + disgust.ToString() + "\"" + "\n" + "fear\":" + fear.ToString() + "\"" + "\n";
            s1 = s1 + "happiness\":" + happiness.ToString() + "\"" + "\n" + "neutral\":" + neutral.ToString() + "\"" + "\n";
            s1 = s1 + "sadness\":" + sadness.ToString() + "\"" + "\n" + "surprise\":" + surprise.ToString() + "\"" + "\n";
            return s1;
        }
    }

    
    
}
