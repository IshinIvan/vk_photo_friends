﻿using Get_Method;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VK_FriendsPhoto
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void OnClick_button_gtfl(object sender, RoutedEventArgs e)
        {
            if (token_save.s_token != "")
            {
                ProgressBarWindow progresswindow = new ProgressBarWindow();
                progresswindow.Show();

                List<User_object> users = new List<User_object>();

                string s1 = await Get_Method.GetClass.GetURLContentsAsync(vk_helper.s_url + vk_helper.s_get_url, vk_helper.s_pr + token_save.s_user_id + vk_helper.s_order + vk_helper.s_vers + "&access_token=" + token_save.s_token);
                //Json
                json_for_friends s_for_json_friends = JsonConvert.DeserializeObject<json_for_friends>(s1);

                //TestWindow testwindow = new TestWindow();

                if (s_for_json_friends.response != null)
                {
                    int count_photo = 0;
                    string s_for_small_url = "";
                    string s_for_user_large_photo = "";
                    for (int i = 0; i < s_for_json_friends.response.count; i++)
                    {
                        s_for_small_url = vk_helper.s_pr + s_for_json_friends.response.items[i].ToString() + "&fields=" + vk_helper.s_photo_max + "&" + vk_helper.s_photo_100 + vk_helper.s_vers;
                        s_for_user_large_photo = await Get_Method.GetClass.GetURLContentsAsync(vk_helper.s_url + vk_helper.s_get_url_ids, s_for_small_url);
                        json_for_user_ids s_for_json_user_ids = JsonConvert.DeserializeObject<json_for_user_ids>(s_for_user_large_photo);
                        //testwindow.textBox_test.Text = testwindow.textBox_test.Text + "\n" + s_for_json_user_ids.response[0].photo_max;
                        if ((s_for_json_user_ids.response[0].photo_max != "") & (count_photo <= 13))
                        {
                            count_photo++;
                            string stringFinalResult = await Post_Method.PostClass.PostAsync(face_helper.s_for_url_face, "{\"url\": \"" + s_for_json_user_ids.response[0].photo_max + "\"}", face_helper.s_key_for_emote);
                            if (stringFinalResult != "[]")
                            {
                                User_object user = new User_object();
                                Req_emote[] final_result = JsonConvert.DeserializeObject<Req_emote[]>(stringFinalResult);
                                //testwindow.textBox_test.Text = testwindow.textBox_test.Text + s_for_json_user_ids.response[0].first_name + " " + s_for_json_user_ids.response[0].last_name + "\n" + final_result[0].faceRectangle.trans_faceRectangle() + final_result[0].scores.trans_scores() + "\n";

                                // testwindow.textBox_test.Text = testwindow.textBox_test.Text + "\n" + Post_Method.PostClass.POST(face_helper.s_for_url_face, "{\"url\": \"" + s_for_json_user_ids.response[0].photo_max + "\"}", face_helper.s_key_for_emote);


                                user.first_name = s_for_json_user_ids.response[0].first_name;
                                user.last_name = s_for_json_user_ids.response[0].last_name;
                                user.Image_Url = s_for_json_user_ids.response[0].photo_max;
                                user.face_rectangle = final_result[0].faceRectangle.trans_faceRectangle();
                                user.trans_scores = final_result[0].scores.trans_scores();

                                users.Add(user);
                            }

                        }
                    }

                    Window_for_objects win_for_obj = new Window_for_objects(users);
                    win_for_obj.Show();
                    progresswindow.Close();
                    this.Close();
                    


                }
                else
                {
                    Error_window error_window = new Error_window();
                    error_window.Show();
                    error_window.error_textBox.Text = "Response is null. Please try again after this window will be closed";
                    Timer class_timer = new Timer(45, error_window);
                }
            }
            else
            {
                Error_window error_window = new Error_window();
                error_window.Show();
                error_window.error_textBox.Text = "Please log in";
                Timer class_timer = new Timer(15, error_window);
            }
        }

        private void auth_button_Click(object sender, RoutedEventArgs e)
        {
            auth_window openpage = new auth_window();
            openpage.Show();
        }
    }
}
