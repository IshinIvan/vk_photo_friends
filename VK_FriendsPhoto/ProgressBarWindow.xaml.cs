﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VK_FriendsPhoto
{
    /// <summary>
    /// Логика взаимодействия для ProgressBarWindow.xaml
    /// </summary>
    public partial class ProgressBarWindow : Window
    {
        public ProgressBarWindow()
        {
            InitializeComponent();
          //  loadProgressBarAnimation();
        }

        private void loadProgressBarAnimation()
        {
            Duration duration = new Duration(TimeSpan.FromSeconds(30));
            DoubleAnimation doubleAnimation = new DoubleAnimation(0, 200, duration);
            ProgressBarAnimation.BeginAnimation(ProgressBar.ValueProperty, doubleAnimation);
        }
    }
}
